<?php
session_start();
require_once('../include/fonction.php');
require_once('../include/connexion.php');
?>

<!-- La partie HTML (= ce qu'on voit en tant qu'utilisateur) débute en-dessous -->
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>Index</title>
    <link href="../include/style.css" rel="stylesheet">
    <link href="../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <!-- On inclut le fichier menu qui s'affiche en haut de page -->
    <?php
    require_once("../include/menu.php");
    echo afficheMessages();
    ?>
    <div class="container">

        <?php
        /**
         * Page qui affiche la liste de toutes les pages du site
         */

        echo "<h1>Le meilleur site de la classe</h1>";
        ?>

        <img src="https://usagif.com/wp-content/uploads/2021/4fh5wi/bienvn-16.gif" alt="">
        <br>Vous êtes actuellement sur le meilleur site TP de la classe des B1 de l'ESGI de Grenoble ! Je vous souhaite une très bonne navigation !

    </div>
</body>

</html>