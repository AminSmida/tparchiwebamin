<!-- On inclut le fichier connexion et fonction -->
<?php
session_start();
require_once('../include/connexion.php');
require_once('../include/fonction.php');
$id = (isset($_GET['id'])) ? $_GET['id'] : 0;
if (!isset($_POST['Nouveau']) && (!isset($_POST['Annuler'])) && (!isset($_POST['Creer'])) && ($id == 0)) {
    header("Location:$url/listeFournisseur.php");
    die();
}

// Initialisation de MSG_KO (= erreur si la condition n'est pas respectée)
$_SESSION['MSG_KO'] = "";

// Si on clique sur "Annuler", on retourne à la liste des fournisseurs
if (isset($_POST['Annuler'])) {
    header("Location: ./listeFournisseur.php");
}

// Si on clique sur "Modifier" ou "Créer"
if (isset($_POST['Modifier']) or isset($_POST['Creer'])) {

    // Contrôle de saisie afin d'imposer certaines conditions avant la modification d'un fournisseur
    // "trim" supprime les espaces au début et à la fin du nom afin de ne pas les considérer comme un caractère
    $nom = trim($_POST['nom']);
    $adresse1 = trim($_POST['adresse1']);

    // On ajoute une condition afin que le champ "Nom" contienne au moins 3 caractères lors de la modification
    if (empty($nom) || strlen($nom) < 3) {
        $_SESSION['MSG_KO'] .= "Le nom du fournisseur est obligatoire et doit contenir au minimum 3 caractères<br>";
    }

    // On ajoute une condition afin que le champ "Adresse" contienne au moins 1 caractère lors de la modification
    if (empty($adresse1) || strlen($adresse1) < 1) {
        $_SESSION['MSG_KO'] .= "L'adresse est obligatoire<br>";
    }

    // Contrôle de saisie avancée afin que le nom du fournisseur choisi soit unique
    $requete = $bdd->prepare('select count(*) as cpt from fournisseur where nom = ? ');
    $requete->execute(array($_POST['nom']));
    $compteur = $requete->fetch();
    if ($compteur['cpt'] == 1) {
        $_SESSION['MSG_KO'] .= "le nom (" . $_POST['nom'] . ") est déjà pris<br />";
    }

    // S'il n'y a pas d'erreur, la modification s'effectue (avec un message OK). Sinon, la modification ne s'effectue pas (avec un message KO)
    if (isset($_POST['Modifier']) and empty($_SESSION['MSG_KO']) and $_SESSION['fournisseur'] == 1) {
        try {
            $requete = $bdd->prepare('update fournisseur
                set nom = :nom
        , adresse1 = :adresse1
        , adresse2 = :adresse2
        , ville = :ville
        , contact = :contact
        , civilite = :civilite
        , code = :code
        where code = :code');
            $requete->execute(array(
                'nom' => $_POST['nom'], 'adresse1' => $_POST['adresse1'], 'adresse2' => $_POST['adresse2'], 'ville' => $_POST['ville'], 'contact' => $_POST['contact'], 'civilite' => $_POST['civilite'], 'code' => $_POST['code']
            ));
        } catch (PDOException $e) {
            print "Erreur !: " . $e->getMessage() . "<br/>";
            die();
        }
        $_SESSION['MSG_OK'] = "Modification bien enregistrée";
    }

    // Si on clique sur "Créer" et qu'il n'y a pas d'erreur
    if (isset($_POST['Creer']) and empty($_SESSION['MSG_KO']) and $_SESSION['fournisseur'] == 1) {
        try {
            $requete = $bdd->prepare('insert into fournisseur (nom, adresse1, adresse2,
   ville, contact, civilite) values
    (:nom, :adresse1, :adresse2, :ville, :contact, :civilite)');
            $requete->execute(array(
                'nom' => $_POST['nom'], 'adresse1' => $_POST['adresse1'], 'adresse2' => $_POST['adresse2'], 'ville' => $_POST['ville'], 'contact' => $_POST['contact'], 'civilite' => $_POST['civilite']
            ));
            // On met un message de succès
            $_SESSION['MSG_OK'] = "Création bien enregistrée";
            // On récupère l'ID du fournisseur créé
            $id = $bdd->lastInsertId();
        } catch (PDOException $e) {
            print "Erreur !: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    // En cas d’erreur, on reste en mode création
    if (isset($_POST['Creer'])) {
        $_POST['Nouveau'] = 'Nouveau';
    }
}

// Si on clique sur "Supprimer", le fournisseur sélectionné se supprime et nous sommes redirigé vers la liste des fournisseurs. En cas d'erreur de suppression, un message d'erreur s'affiche
if (isset($_POST['Supprimer']) and $_SESSION['fournisseur'] == 1) {
    try {
        $requete = $bdd->prepare('delete from fournisseur where code = ?');
        $requete->execute(array($_POST['code']));
        $_SESSION['MSG_OK'] = "Suppression bien enregistrée";
        header("Location: ./listeFournisseur.php");
        exit();
    } catch (PDOException $e) {
        print "Erreur !:" . $e->getMessage() . "<br/>";
        echo afficheMessages($_SESSION['MSG_KO'] = "Suppression non effectuée");
        die();
    }
}

// Requête SQL qui nous donne, depuis la base de données, le nom, l'adresse, la suite d'adresse, la ville, le contact et la civilité d'un fournisseur et nous l'affiche (sauf le code) dans le formulaire (voir partie HTML)
try {
    $requete = $bdd->prepare('select nom, adresse1, adresse2, ville, contact,
civilite, code from fournisseur where code = ?');
    $requete->execute(array($id));
    $fournisseur = $requete->fetch();
} catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
}
?>

<!-- La partie HTML (= ce qu'on voit en tant qu'utilisateur) débute en-dessous -->
<!DOCTYPE html>
<html lang="fr">

<!-- En-tête de la page -->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Pour que le titre de la page s'affiche selon si on créer ou on édite un fournisseur -->
    <title>
        <?php if (isset($fournisseur['nom'])) {
            echo $fournisseur['nom'];
        } else {
            echo "Nouveau fournisseur";
        }
        ?>
    </title>
    <link href="../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../include/style.css" rel="stylesheet">
</head>

<body>
    <!-- Permet d'avoir la barre de navigation en haut de page, le message OK / KO en cas de modification / création / suppression et le titre de page selon si on est en édition ou création -->
    <?php
    include("../include/menu.php");
    echo afficheMessages();
    $mode = ($id == 0) ? "Nouveau fournisseur" : "Fournisseur : ";
    ?>

    <div class="container mt-2">
        <form method="post" class="row g-3">
            <div class="container mt-5">
                <!-- En mode création, il y aura écrit "Nouveau fournisseur" et en mode édition, il y aura écrit "Fournisseur [nom du fournisseur]" -->
                <h1><?php echo $mode; ?> <?php echo $fournisseur['nom'] ?? ''; ?></h1>
            </div>

            <!-- Formulaire de la page, ce qui permet d'afficher les cases Nom, Adresse, Suite adresse, Ville, Contact et Civilité (ainsi que Code mais il est invisible puisqu'inutile pour l'utilisateur) -->
            <div class="form-group mb-3">
                <label class="col-form-label col-sm-2" for="nom">Nom</label>
                <div class="col-sm-5">
                    <input class="form-control" id="nom" name="nom" value="<?php echo $fournisseur['nom'] ?? ''; ?>">
                </div>
            </div>

            <div class="form-group mb-3">
                <label class="col-form-label col-sm-2" for="adresse1">Adresse</label>
                <div class="col-sm-5">
                    <input class="form-control" id="adresse1" name="adresse1" value="<?php echo $fournisseur['adresse1'] ?? ''; ?>">
                </div>
            </div>

            <div class="form-group mb-3">
                <label class="col-form-label col-sm-2" for="adresse2">Suite adresse</label>
                <div class="col-sm-5">
                    <input class="form-control" id="adresse2" name="adresse2" value="<?php echo $fournisseur['adresse2'] ?? ''; ?>">
                </div>
            </div>

            <div class="form-group mb-3">
                <label class="col-form-label col-sm-2" for="ville">Ville</label>
                <div class="col-sm-5">
                    <?php echo selectVille('ville', $fournisseur['ville'] ?? ''); ?>
                </div>
            </div>

            <div class="form-group mb-3">
                <label class="col-form-label col-sm-2" for="contact">Contact</label>
                <div class="col-sm-5">
                    <input class="form-control" id="contact" name="contact" value="<?php echo $fournisseur['contact'] ?? ''; ?>">
                </div>
            </div>

            <div class="form-group mb-3">
                <label class="col-form-label col-sm-2" for="civilite">Civilité</label>
                <div class="col-sm-5">
                    <?php echo selectCivilite('civilite', $fournisseur['civilite'] ?? ''); ?>
                </div>
            </div>

            <div class="col-sm-5">
                <input type="hidden" class="form-control" id="code" name="code" value="<?php echo $fournisseur['code'] ?? ''; ?>">
            </div>

            <!-- Affichage des boutons Annuler, Supprimer et Modifier selon si on est connecté en administrateur, toto ou si on est pas connecté -->
            <div class="form-group row float-right">
                <?php if (isset($_POST['Nouveau'])) {
                    echo '<input type="submit" class="btn btn-primary" name="Creer" value="Créer">';
                } else {
                    if (isset($_SESSION['fournisseur']) and $_SESSION['fournisseur'] == 1) {
                        echo '<input type="submit" class="btn btn-primary" name="Modifier" value="Modifier">
                        <input type="submit" class="btn btn-danger confirm" name="Supprimer" value="Supprimer">';
                    }
                }
                ?>
                <input type="submit" class="btn btn-secondary" name="Annuler" value="Annuler">
            </div>
        </form>
    </div>

    <!-- Script qui permet d'avoir un pop-up de confirmation de suppression -->
    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script>
        $(function() {
            $('.confirm').click(function() {
                return window.confirm("Êtes-vous sur ?");
            });
        });
    </script>
</body>

</html>