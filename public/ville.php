<!-- On inclut le fichier connexion et fonction, qui nous sera utile plus tard pour appelées différentes fonctions -->
<?php
session_start();
require_once('../include/connexion.php');
require_once('../include/fonction.php');
$id = (isset($_GET['id'])) ? $_GET['id'] : 0;
if (!isset($_POST['Nouveau']) && (!isset($_POST['Annuler'])) && (!isset($_POST['Creer'])) && ($id == 0)) {
    header("Location:$url/listeVille.php");
    die();
}

// Initialisation de MSG_KO (= erreur si la condition n'est pas respectée)
$_SESSION['MSG_KO'] = "";

// Si on clique sur "Annuler", on retourne à la liste des villes
if (isset($_POST['Annuler'])) {
    header("Location: ./listeVille.php");
}

// Si on clique sur "Modifier" ou "Créer"
if (isset($_POST['Modifier']) or isset($_POST['Creer'])) {

    // Contrôle de saisie afin d'imposer certaines conditions avant la modification d'une ville
    // "trim" supprime les espaces au début et à la fin du nom afin de ne pas les considérer comme un caractère
    $nom = trim($_POST['nom']);
    $codepostal = trim($_POST['codepostal']);
    $pays = trim($_POST['pays']);

    // On ajoute une condition afin que le champ "Nom" contienne au moins 3 caractères lors de la modification
    if (empty($nom) || strlen($nom) < 2) {
        $_SESSION['MSG_KO'] .= "Le nom de la ville est obligatoire et doit contenir au minimum 2 caractères<br>";
    }

    // On ajoute une condition afin que le champ "Code postal" contienne 5 chiffres lors de la modification
    if (empty($codepostal) || strlen($codepostal) != 5) {
        $_SESSION['MSG_KO'] .= "Le code postal doit contenir 5 chiffres<br>";
    }

    // On ajoute une condition afin que le champ "Pays" doit être dans la liste « France », « Andorre » ou « Monaco »
    $array = ['France', 'france', 'Andorre', 'andorre', 'Monaco', 'monaco'];
    if (!in_array($pays, $array)) {
        $_SESSION['MSG_KO'] .= "Le pays doit être dans la liste « France », « Andorre » ou « Monaco »<br>";
    }

    // Contrôle de saisie avancée afin que le nom de la ville choisie soit unique
    $requete = $bdd->prepare('select count(*) as cpt from ville where nom = ? and code != ? ');
    $requete->execute(array($_POST['nom'], $_POST['code']));
    $compteur = $requete->fetch();
    if ($compteur['cpt'] == 1) {
        $_SESSION['MSG_KO'] .= "le nom (" . $_POST['nom'] . ") est déjà pris<br />";
    }

    // S'il n'y a pas d'erreur, la modification s'effectue (avec un message OK). Sinon, la modification ne s'effectue pas (avec un message KO)
    if (isset($_POST['Modifier']) and empty($_SESSION['MSG_KO']) and $_SESSION['ville'] == 1) {
        try {
            $requete = $bdd->prepare('update ville
            set nom = :nom
            , codepostal = :codepostal
            , pays = :pays
            , code = :code
            where code = :code');
            $requete->execute(array(
                'nom' => $_POST['nom'], 'codepostal' => $_POST['codepostal'], 'pays' => $_POST['pays'], 'code' => $_POST['code']
            ));
        } catch (PDOException $e) {
            print "Erreur !: " . $e->getMessage() . "<br/>";
            die();
        }
        $_SESSION['MSG_OK'] = "Modification bien enregistrée";
    }

    // Si on clique sur "Créer" et qu'il n'y a pas d'erreur
    if (isset($_POST['Creer']) and empty($_SESSION['MSG_KO']) and $_SESSION['ville'] == 1) {
        try {
            $requete = $bdd->prepare('insert into ville (nom, codepostal, pays) values
    (:nom, :codepostal, :pays)');
            $requete->execute(array(
                'nom' => $_POST['nom'], 'codepostal' => $_POST['codepostal'], 'pays' => $_POST['pays']
            ));
            // On met un message de succès
            $_SESSION['MSG_OK'] = "Création bien enregistrée";
            // On récupère l'id de la ville créée
            $id = $bdd->lastInsertId();
        } catch (PDOException $e) {
            print "Erreur !: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    // En cas d’erreur, on reste en mode création
    if (isset($_POST['Creer'])) {
        $_POST['Nouveau'] = 'Nouveau';
    }
}

// Si on clique sur "Supprimer", la ville sélectionnée se supprime et nous sommes redirigé vers la liste des villes. En cas d'erreur de suppression, un message d'erreur s'affiche
if (isset($_POST['Supprimer']) and $_SESSION['ville'] == 1) {
    try {
        $requete = $bdd->prepare('delete from ville where code = ?');
        $requete->execute(array($_POST['code']));
        $_SESSION['MSG_OK'] = "Suppression bien enregistrée";
        header("Location: ./listeVille.php");
        exit();
    } catch (PDOException $e) {
        print "Erreur !:" . $e->getMessage() . "<br/>";
        echo afficheMessages($_SESSION['MSG_KO'] = "Suppression non effectuée");
        die();
    }
}

// Requête SQL qui nous donne, depuis la base de données, le code, le nom, le code postal et le pays d'une commune et nous l'affiche (sauf le code) dans le formulaire (voir partie HTML)
try {
    $requete = $bdd->prepare('select nom, codepostal, pays, code
    from ville where code = ?');
    $requete->execute(array($id));
    $ville = $requete->fetch();
} catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
}

// La partie HTML (= ce qu'on voit en tant qu'utilisateur) débute en-dessous
?>
<!DOCTYPE html>
<html lang="fr">

<!-- En-tête de la page -->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Pour que le titre de la page s'affiche selon si on créer ou on édite une ville -->
    <title>
        <?php if (isset($ville['nom'])) {
            echo $ville['nom'];
        } else {
            echo "Nouvelle ville";
        }
        ?>
    </title>
    <link href="../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../include/style.css" rel="stylesheet">
</head>

<body>
    <!-- Permet d'avoir la barre de navigation en haut de page et le message OK / KO en cas de modification -->
    <?php
    include('../include/menu.php');
    echo afficheMessages();
    $mode = ($id == 0) ? "Nouvelle ville" : "Ville : ";
    ?>

    <div class="container mt-2">
        <form method="post" class="row g-3">
            <div class="container mt-5">
                <!-- En mode création, il y aura écrit "Nouvelle ville" et en mode édition, il y aura écrit "Ville [nom de la ville]" -->
                <h1><?php echo $mode; ?> <?php echo $ville['nom'] ?? ''; ?></h1>
            </div>

            <!-- Formulaire de la page, ce qui permet d'affiché les cases Nom, Code postal et Pays (ainsi que Code mais il est invisible puisqu'inutile pour l'utilisateur) -->
            <div class="form-group mb-3">
                <label class="col-form-label col-sm-2" for="nom">Nom</label>
                <div class="col-sm-5">
                    <input class="form-control" id="nom" name="nom" value="<?php echo $ville['nom'] ?? ''; ?>">
                </div>
            </div>

            <div class="form-group mb-3">
                <label class="col-form-label col-sm-2" for="codepostal">Code postal</label>
                <div class="col-sm-5">
                    <input class="form-control" id="codepostal" name="codepostal" value="<?php echo $ville['codepostal'] ?? ''; ?>">
                </div>
            </div>

            <div class="form-group mb-3">
                <label class="col-form-label col-sm-2" for="pays">Pays</label>
                <div class="col-sm-5">
                    <input class="form-control" id="pays" name="pays" value="<?php echo $ville['pays'] ?? ''; ?>">
                </div>
            </div>

            <div class="col-sm-5">
                <input type="hidden" class="form-control" id="code" name="code" value="<?php echo $ville['code'] ?? ''; ?>">
            </div>

            <!-- Affichage des boutons Annuler, Supprimer et Modifier selon si on est connecté en administrateur, toto ou si on est pas connecté -->
            <div class="form-group row float-right">
                <?php if (isset($_POST['Nouveau'])) {
                    echo '<input type="submit" class="btn btn-primary" name="Creer" value="Créer">';
                } else {
                    if (isset($_SESSION['ville']) and $_SESSION['ville'] == 1) {
                        echo '<input type="submit" class="btn btn-primary" name="Modifier" value="Modifier">
                        <input type="submit" class="btn btn-danger confirm" name="Supprimer" value="Supprimer">';
                    }
                }
                ?>
                <input type="submit" class="btn btn-secondary" name="Annuler" value="Annuler">
            </div>
        </form>
    </div>

    <!-- Script qui permet d'avoir un pop-up de confirmation de suppression -->
    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script>
        $(function() {
            $('.confirm').click(function() {
                return window.confirm("Êtes-vous sur ?");
            });
        });
    </script>
</body>

</html>