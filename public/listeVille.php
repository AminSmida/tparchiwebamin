<!-- La partie HTML (= ce qu'on voit en tant qu'utilisateur) débute en-dessous -->
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>Liste des villes</title>
    <link href="../include/style.css" rel="stylesheet">
    <link href="../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <!-- On inclut le fichier connexion, menu et fonction -->
    <?php
    session_start();
    require_once("../include/connexion.php");
    require_once("../include/menu.php");
    require_once('../include/fonction.php');
    echo afficheMessages(); ?>

    <div class="container">

        <?php

        /**
         * Page qui affiche la liste de tout les villes
         */

        echo "<h1 class='mt-3 mb-3'>Les villes</h1>";

        $requete = "SELECT code, nom, codepostal, pays
            FROM ville";

        ?>

        <table id="villes" class="table table-striped display">
            <thead>
                <tr>
                    <th>Nom</th>
                    <th>Code postal</th>
                    <th>Pays</th>
                </tr>
            </thead>
            <tbody>

                <?php
                try {
                    foreach ($bdd->query($requete) as $ligne) {
                        echo '<tr class = "clickable-row" data-href = "ville.php?id=' . $ligne['code'] . '">';
                        echo '<td>' . $ligne['nom'] . '</td>';
                        echo '<td>' . $ligne['codepostal'] . '</td>';
                        echo '<td>' . $ligne['pays'] . "</td>\n";
                    }
                } catch (PDOException $e) {
                    echo 'Erreur !: ' . $e->getMessage() . '<br/>';
                    die();
                }
                ?>

                </tr>
            </tbody>

        </table>
    </div>
    <?php
    if (isset($_SESSION['ville']) and $_SESSION['ville'] == 1) {
        // On ne met le bouton "Nouveau" que si l'utilisateur a le droit
    ?>

        <div class="container">
            <form method="post" action="ville.php">
                <div class="form-group row float-right">
                    <input type="submit" class="btn btn-primary" name="Nouveau" value="Nouveau">
                </div>
            </form>
        </div>
    <?php
    }
    ?>


    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../node_modules/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../node_modules/datatables.net-bs5/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#villes').DataTable({
                language: {
                    url: '../include/fr_FR.json'
                }
            });
        });
    </script>
    <script>
        $(document).ready(function($) {
            $(".clickable-row").click(function() {
                window.location = $(this).data("href");
            });
        });
    </script>

</body>

</html>