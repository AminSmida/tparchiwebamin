<!-- La partie HTML (= ce qu'on voit en tant qu'utilisateur) débute en-dessous -->
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>Liste des fournisseurs</title>
    <link href="../include/style.css" rel="stylesheet">
    <link href="../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <!-- On inclut le fichier connexion, menu et fonction -->
    <?php
    session_start();
    require_once("../include/connexion.php");
    require_once("../include/menu.php");
    require_once('../include/fonction.php');
    echo afficheMessages(); ?>

    <div class="container">

        <?php

        /**
         * Page qui affiche la liste de tout les fournisseurs
         */

        echo "<h1 class='mt-3 mb-3'>Les fournisseurs</h1>";

        $requete = "SELECT f.code
            , c.libelle
            , f.nom
            , f.contact
            , v.codepostal
            , v.nom as ville
            FROM fournisseur f
            , civilite c
            , ville v
            WHERE f.civilite = c.code
            AND f.ville = v.code";
        ?>

        <table id="fournisseurs" class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Civilité</th>
                    <th>Nom</th>
                    <th>Contact</th>
                    <th>Code postal</th>
                    <th>Ville</th>
                </tr>
            </thead>
            <tbody>

                <?php
                try {
                    foreach ($bdd->query($requete) as $ligne) {
                        echo '<tr class = "clickable-row" data-href = "fournisseur.php?id=' . $ligne['code'] . '">';
                        echo '<td>' . $ligne['nom'] . '</td>';
                        echo '<td>' . $ligne['libelle'] . '</td>';
                        echo '<td>' . $ligne['contact'] . '</td>';
                        echo '<td>' . $ligne['codepostal'] . '</td>';
                        echo '<td>' . $ligne['ville'] . "</td>\n";
                    }
                } catch (PDOException $e) {
                    echo 'Erreur !: ' . $e->getMessage() . '<br/>';
                    die();
                }
                ?>

                </tr>
            </tbody>
        </table>
        <?php
        if (isset($_SESSION['fournisseur']) and $_SESSION['fournisseur'] == 1) {
            // On ne met le bouton "Nouveau" que si l'utilisateur a le droit
        ?>
            <div class="container">
                <form method="post" action="fournisseur.php">
                    <div class="form-group row float-right">
                        <input type="submit" class="btn btn-primary" name="Nouveau" value="Nouveau">
                    </div>
                </form>
            </div>

        <?php
        }
        ?>

    </div>

    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../node_modules/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../node_modules/dataTables.net-bs5/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#fournisseurs').DataTable({
                language: {
                    url: '../include/fr_FR.json'
                }
            });
        });
    </script>
    <script>
        $(document).ready(function($) {
            $(".clickable-row").click(function() {
                window.location = $(this).data("href");
            });
        });
    </script>

</body>

</html>