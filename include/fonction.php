<?php

/**
 * Fonction qui génère un input type select avec toutes les villes
 * @param $id id du select
 * @param $code id de la ville choisie
 * @return code HTML à afficher
 */
function selectVille($id, $code)
{
    global $bdd;
    $retour = "<select class=\"form-select\" id=\"$id\" name=\"$id\">\n";
    try {
        $requete = 'select code, nom from ville';
        foreach ($bdd->query($requete) as $ligne) {
            if ($ligne['code'] == $code) {
                $retour .= "<option value=" . $ligne['code'] . ' selected="selected">' . $ligne['nom'] . "</option>\n";
            } else {
                $retour .= "<option value=" . $ligne['code'] . ">" . $ligne['nom'] . "</option>\n";
            }
        }
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage() . "<br/>";
        die();
    }
    $retour .= "</select>";
    return $retour;
}

/**
 * Fonction qui génère un input type select avec toutes les civilités
 * @param $id id du select
 * @param $code code de la civilité choisie
 * @return code HTML à afficher
 */
function selectCivilite($id, $code)
{
    global $bdd;
    $retour = "<select class=\"form-select\" id=\"$id\" name=\"$id\">\n";
    try {
        $requete = 'select code, libelle from civilite';
        foreach ($bdd->query($requete) as $ligne) {
            if ($ligne['code'] == $code) {
                $retour .= "<option value=" . $ligne['code'] . ' selected="selected">' . $ligne['libelle'] . "</option>\n";
            } else {
                $retour .= "<option value=" . $ligne['code'] . ">" . $ligne['libelle'] . "</option>\n";
            }
        }
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage() . "<br/>";
        die();
    }
    $retour .= "</select>";
    return $retour;
}

/**
 * Fonction qui génère la classe 'active' si le menu est sélectionné
 * @param $menu Nom du menu d'appel
 * @return code HTML à afficher
 */
function menuActif($menu)
{
    $ecran = basename($_SERVER['SCRIPT_FILENAME'], ".php");
    if ($menu == $ecran) {
        return "active";
    } else {
        return "";
    }
}

/**
 * Fonction qui affiche un message si une modification a été effectuée
 * @return code HTML à afficher
 */
function afficheMessages()
{
    $retour = '';
    if (!empty($_SESSION['MSG_OK'])) {
        $retour .= '<div class="alert alert-success">' . $_SESSION['MSG_OK'] .
            '</div>' . "\n";
        unset($_SESSION['MSG_OK']);
    } elseif (!empty($_SESSION['MSG_KO'])) {
        $retour .= '<div class="alert alert-danger">' . $_SESSION['MSG_KO'] .
            '</div>' . "\n";
        unset($_SESSION['MSG_KO']);
    }
    return $retour;
}

/**
 * Fonction qui génère le formulaire de connexion
 * @return code HTML à afficher
 */
function formulaireLogin()
{
    if (isset($_SESSION['code'])) {
        // Si on est connecté
        $html = '<form class="form-inline" method="post">
        <div class="form-group">
        <label>Bienvenue ' . $_SESSION['login'] . '&nbsp;</label>
        <button type="submit" class="btn btn-primary" name="Deconnexion">Se
       déconnecter</button>
        </div>
        </form>';
    } else {
        // Si on n'est pas connecté    
        $html = '<form class="form-inline d-flex" method="post">';
        $html .= '<div class="input-group">';
        $html .= '<input type="text" class="form-control" id="login" name="login" placeholder="Identifiant">';
        $html .= '<input type="password" class="form-control" id="password" name="password" placeholder="Mot de passe">';
        $html .= '<button type="submit" class="btn btn-primary" name="Connexion">Se connecter</button>';
        $html .= '</div>';
        $html .= '</form>';
    }
    return $html;
}
