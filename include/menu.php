<!-- On inclut le fichier fonction -->
<?php
require_once('../include/fonction.php');
?>

<?php
global $bdd;
// Gestion de la connexion
if (isset($_POST['Connexion'])) {
  try {
    $requete = $bdd->prepare('select code, password, ville, fournisseur from
utilisateur where login = lower(?)');
    $requete->execute(array($_POST['login']));
    $utilisateur = $requete->fetch();
  } catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
  }
  // Si le login est inconnu
  if (empty($utilisateur['code'])) {
    // On met un message d'erreur générique
    $_SESSION['MSG_KO'] = "Identification invalide";
  } else {
    // Test du mot de passe saisi
    if (password_verify($_POST['password'], $utilisateur['password'])) {
      $_SESSION['login'] = $_POST['login'];
      $_SESSION['code'] = $utilisateur['code'];
      $_SESSION['ville'] = $utilisateur['ville'];
      $_SESSION['fournisseur'] = $utilisateur['fournisseur'];
      $_SESSION['MSG_OK'] = "Bienvenue";
    } else {
      // On met le même message d'erreur que pour "login raté"
      $_SESSION['MSG_KO'] = "Identification invalide";
    }
  }
}
// Gestion de la déconnexion
if (isset($_POST['Deconnexion'])) {
  unset($_SESSION['login']);
  unset($_SESSION['code']);
  unset($_SESSION['ville']);
  unset($_SESSION['fournisseur']);
  $_SESSION['MSG_OK'] = "Vous êtes bien déconnecté";
}
?>

<!-- Affichage de la barre de navigation -->
<nav class="navbar navbar-expand-lg" style="background-color: #e3f2fd;">
  <div class="container-fluid">
    <a href="../public/index.php"><img src="https://www.radiofrance.fr/s3/cruiser-production/2019/10/796598e0-2d78-492d-9b08-ad2ec8188c2c/870x489_shrek-et-le-chat-potte-reviennent-bientot-au-cinema-big.jpg" style="width : 75px;" alt=""></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link <?php echo menuActif('listeFournisseur'); ?>" aria-current="page" href="<?php echo (PATH); ?>listeFournisseur.php">Fournisseurs</a>
        </li>
      </ul>
      <div class="collapse navbar-collapse">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link <?php echo menuActif('listeVille'); ?>" aria-current="page" href="<?php echo (PATH); ?>listeVille.php">Villes</a>
          </li>
        </ul>
      </div>
    </div>

    <!-- Affichage du formulaire de connexion -->
    <?php echo formulaireLogin(); ?>
  </div>
</nav>